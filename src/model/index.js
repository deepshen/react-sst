import homepage from './homepage';
import ceshi from './ceshi';

const model = [
  homepage,
  ceshi,
];

export default model;
