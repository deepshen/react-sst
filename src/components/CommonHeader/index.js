import React from 'react';
import { Menu } from 'antd';
import Link from 'next/link';

import head from '@/static/head.jpg';

import './index.less';

function CommonHeader() {
  return (
    <div className="head-box">
      <div className="logo">
        <img src={head} alt="touxiang" />
        测试
      </div>
      <Menu
        theme="dark"
        mode="horizontal"
        defaultOpenKeys={['1']}
      >
        <Menu.Item key="1">
          <Link href="/">
            <a>nava1</a>
          </Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link href="/test">
            <a>nav2</a>
          </Link>
        </Menu.Item>
        <Menu.Item key="3">
          nav 3
        </Menu.Item>
      </Menu>
    </div>
  );
}

export default CommonHeader;
