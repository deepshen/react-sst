
export const isPc = () => {
  const userAgentInfo = window.navigator.userAgent;
  const reg = /(Android | iPhone | SymbianOs | Window Phone | iPad | iPod)/g;
  if (reg.test(userAgentInfo)) {
    return false;
  }
  return true;
};
