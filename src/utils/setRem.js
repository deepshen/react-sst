export default function setRem() {
  const ww = document.body.clientWidth;
  const rw = ww > 768 ? 768 : ww;
  const fts = rw * 100 / 750;
  document.documentElement.style.fontSize = `${fts}px`;
}
