import App from 'next/app';
import React from 'react';
import { Layout } from 'antd';
import CommonHeader from '@/components/CommonHeader';
import setRem from '@/utils/setRem';
import Head from 'next/head';
import './style.less';

const { Header, Content, Footer } = Layout;

class MyApp extends App {
  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  handleResize = () => {
    console.log(12342);
    setRem();
  }

  componentDidMount() {
    console.log(123);
    setRem();
    try {
      window.addEventListener('resize', this.handleResize);
    } catch (e) {
      console.warn('not window');
    }
  }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <>
        <Head>
          <title>hello world</title>
          <link rel="icon" href="/favicon.ico" />
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        </Head>
        <Layout>
          <Header>
            <CommonHeader />
          </Header>
          <Content>
            <Component {...pageProps} />
          </Content>
        </Layout>
      </>
    );
  }
}
export default MyApp;
