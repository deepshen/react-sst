import React from 'react';
import Head from 'next/head';
import { Carousel } from 'antd-mobile';
import { Row, Col } from 'antd';
import { twoLayout } from '@/constans/layout';

import './indexstyle.less';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      test: [1, 2, 3],
    };
  }

  render() {
    const { test } = this.state;
    return (
      <div className="container">
        <Head>
          <title>首页1</title>
        </Head>
        <Carousel
          autoplay={false}
          dots={false}
          infinite
          beforeChange={(from, to) => console.log(`slide from ${from} to ${to}`)}
          afterChange={(index) => console.log('slide to', index)}
        >
          {test.map((val) => (
            <a
              key={val}
              href="http://www.alipay.com"
              style={{ display: 'inline-block', width: '100%', height: 300 }}
            >
              <img
                // src={`https://zos.alipayobjects.com/rmsportal/${val}.png`}
                alt=""
                style={{
                  width: '100%', verticalAlign: 'top', height: 300, backgroundColor: 'red',
                }}
                onLoad={() => {
                  // fire window resize event to change height
                  // window.dispatchEvent(new Event('resize'));
                  // this.setState({ imgHeight: 'auto' });
                }}
              />
            </a>
          ))}
        </Carousel>
        <div className="main">
          <Row
            type="flex"
          >
            <Col {...twoLayout}>
              <div>1.hello world</div>
              <div>2.ceshi yixia</div>
              <div>3.ni guan wo</div>
            </Col>
            <Col {...twoLayout}>
              <div>1.hello world</div>
              <div>2.ceshi yixia</div>
              <div>3.ni guan wo</div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Home;
