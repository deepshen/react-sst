const withLess = require('@zeit/next-less');
const withCss = require('@zeit/next-css');
const withPlugins = require('next-compose-plugins');
const webpack = require('webpack');
// const lessToJS = require('less-vars-to-js');
// const fs = require('fs');
const path = require('path');
// fix: prevents error when .less files are required by node
if (typeof require !== 'undefined') {
  require.extensions['.less'] = () => {};
  require.extensions['.css'] = (file) => {}; // eslint-disable-line
}

// const themeVariables = lessToJS(
//   fs.readFileSync(path.resolve(__dirname, './assets/my-antd.less'), 'utf8'),
// );


module.exports = withPlugins([withLess, withCss], {
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: '[local]___[hash:base64:5]',
  },
  lessLoaderOptions: {
    // 如果是antd就需要，antd-mobile不需要
    javascriptEnabled: true,
    // modifyVars: themeVariables,
  },
  javascriptEnabled: true,
  pageExtensions: ['jsx', 'js'],
  generateBuildId: async () => 'gxm',
  webpack: (config, { isServer }) => {
    if (isServer) {
      // 处理ant打包less
      const antStyles = /antd\/.*?\/style.*?/;
      const origExternals = [...config.externals];
      // eslint-disable-next-line no-param-reassign
      config.externals = [
        (context, request, callback) => {
          if (request.match(antStyles)) return callback();
          if (typeof origExternals[0] === 'function') {
            origExternals[0](context, request, callback);
          } else {
            callback();
          }
        },
        ...(typeof origExternals[0] === 'function' ? [] : origExternals),
      ];

      config.module.rules.unshift({
        test: antStyles,
        use: 'null-loader',
      });
    }
    // eslint-disable-next-line no-param-reassign
    config.resolve.alias['@'] = path.resolve(__dirname, 'src');
    config.module.rules.push({
      test: /\.(png|jpg|gif)$/,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 8192,
          },
        },
      ],
    });
    config.plugins.push(new webpack.IgnorePlugin(/\/__tests__\//));
    return config;
  },
});
